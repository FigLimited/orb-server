class Player {
    constructor(socketId, game, canvas, team)
    {
        this.x = Math.floor(Math.random() * Math.floor(game.width));
        this.y = Math.floor(Math.random() * Math.floor(game.height));
        this.radius = game.radius;
        this.arcRadius = game.arcRadius;
        this.socketId = socketId;

        this.team = team;

        //  players local viewport dimensions
        this.canvas = canvas

        //  players session score
        this.score = 0;

        //  used to grab energy or recharge team members
        this.power = game.power;

        this.playing = true;
    }
}

Player.prototype.changePlayState = function (isPlaying)
{
    this.playing = isPlaying;
}

Player.prototype.updateCoordinates = function (x, y)
{
    this.x = x;
    this.y = y;
};

Player.prototype.updateZoom = function (zoom)
{
    this.zoom = zoom;
};

Player.prototype.canTouchBody = function (body, arc)
{
    arc = arc || false;
    //  if centers are closer than both radii added together then hit
    let x = Math.abs(this.x - body.x);
    let y = Math.abs(this.y - body.y);
    let h2 = x * x + y * y;
    let localRadius = arc ? this.arcRadius : this.radius;
    let radii = (localRadius + body.radius) * (localRadius + body.radius);
    return radii >= h2;
}

Player.prototype.eat = function (body, game)
{
    var d = game.maxRadius - this.radius
    var p = d * 0.001
    this.radius += p
    this.score += body.radius
    this.power += body.radius
}

module.exports = Player;
