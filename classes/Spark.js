/**
 * Spark
 */
module.exports = class Spark {
    constructor (x, y, r, id) {
        this.x = x;
        this.y = y;
        this.radius = r;
        this.id = id;
    }
}
