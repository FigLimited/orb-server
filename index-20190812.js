require('dotenv').config();
var express = require('express');
var cors = require('cors');
var app = express();
app.use(cors());
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
uuidv4 = require('uuid/v4');
const bcrypt = require('bcrypt');
var request = require("request");

var port = 3000;

var Datastore = require('nedb');
db = {};
db.users = new Datastore('users.db');

server.listen(process.env.PORT || port, function() {
    console.log('Listening on :' + process.env.PORT || port);
});

var Player = require("./classes/Player.js");
var Spark  = require("./classes/Spark.js");

var players = [];
var hash = {};

var sparks = [];

var game = {
    radius: 20,
    maxRadius: 100,
    arcRadius: 200,
    width: 500,
    height: 500,
    sparks: 2
};

var teams = [
    { name: 'Red', hex: '#ff0000' },
    { name: 'Green', hex: '#00ff00' },
    { name: 'Blue', hex: '#0000ff' },
    { name: 'Pink', hex: '#FFB6C1' },
    { name: 'Orange', hex: '#ffa500' },
    { name: 'Black', hex: '#000000' },
];

var weights = [1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 5];

setInterval(heartBeat, 1000);
function heartBeat() {
    //  update players with up-to-date list
    io.emit('heartbeat', { players: players });
}

setInterval(sparkBeat, 1000);
function sparkBeat() {
    //  check if new sparks are needed
    if (sparks.length < game.sparks) {
        var spark = new Spark(
            Math.floor(Math.random() * game.width), 
            Math.floor(Math.random() * game.height),
            weights[Math.floor(Math.random() * weights.length)],
            uuidv4()
        );
        sparks.push(spark);
        io.emit('spawned', spark);
        // console.log(sparks.length);
    }
}

io.sockets.on('connection', function(socket)
{
    console.log('connected with socket id: ' + socket.id);

    /**
     * Register
     */
    socket.on('register', (data) => register(data, socket));

    /**
     * Login
     */
    socket.on('login', login);

    /**
     * START
     */
    socket.on('start', (data) => start(data, socket));

    /**
     * DISCONNECT
     */
    socket.on('disconnect', (_) => disconnect(socket));

    /**
     * UPDATE
     */
    socket.on('update', (data) => update(data, socket));

    
//     /**
//      * UPDATE
//      */
//     socket.on('update', function(data)
//     {
//         if(players[hash[socket.id]] !== undefined) {
//             //  use this to check random movements
//             players[hash[socket.id]].x = data.x;
//             players[hash[socket.id]].y = data.y;
//         }

//         playersInRange(hash[socket.id]);
//     });

//     /**
//      * ATE
//      */
//     socket.on('ate', function(sparkId)
//     {
//         for(var i = sparks.length - 1; i >= 0; i--) {
//             if(sparks[i].id.toString() == sparkId.toString()) {
//                 sparks.splice(i, 1);
//                 break;
//             }
//         }
//     });

});


function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function getRandomTeam() {
    return teams[Math.floor(Math.random() * teams.length)];
}

// function playersInRange(playerIndex) {
//     return true;
// }

function register(data, socket) {

    db.users.loadDatabase();

    // check if user already exists
    db.users.find({ $or: [{ email: data.email }, { username: data.username }] }, function (err, docs) {
        if(docs.length === 0) {
            registerInsert(data, socket);
        } else {
            console.log('registered_error');
            socket.emit('registered_error');
        }
    });
}

/**
 * 
 * @param {*} data 
 * @param {*} socket 
 */
function registerInsert(data, socket) {

    let expiryDate = new Date()
    expiryDate.setHours(expiryDate.getHours() + 24);

    bcrypt.hash(data.password, 10, function(err, hash) {
        // Store hash in your password DB.
        var doc = { 
            username: data.username,
            email: data.email,
            password: hash,
            api_token: uuidv4(),
            premium_expires_at: expiryDate
        };
        db.users.insert(doc, function (err, newDoc) {   // Callback is optional
            console.log(newDoc);
            socket.emit('registered', { api_token: newDoc.api_token })
        });
    });
    db.users.remove({}, { multi: true }, function (err, numRemoved) {
    });

}

function login(data) {
    //  login here
}

function start(data, socket) {
    // console.log(socket);
    let _socket = socket;

    const api_token = data.api_token;

    var player = new Player(
        getRandomInt(game.width),
        getRandomInt(game.height),
        game.radius,
        game.arcRadius,
        socket.id,
        socket.id, // name
        getRandomTeam()
    );

    // console.log('API_TOKEN', data.api_token);
    if (data.api_token == null) {
        request('https://randomuser.me/api/', function(err, res, body) {
            // Do funky stuff with body
            console.log(body.results);
            let json = JSON.parse(body)
            player.username = json.results[0].login.username;
            startReturn(player, _socket);
        });
    } else {
        //check if player exists in db
        db.users.loadDatabase();
        db.users.findOne({ api_token }, function (err, doc) {
            if(doc !== null) {
                player.username = doc.username;
                player.registered = true;
                player.premium_expires_at = doc.premium_expires_at;
            }
            startReturn(player, _socket);
        });
    }
}

function startReturn (player, socket) {
    var returnData = {
        player: player,
        game: game,
        sparks: sparks
    };

    //  send to client
    socket.emit('started', returnData);

    //  create the player on the server
    players.push(player);

    //  quick reference hash
    hash[socket.id] = players.length - 1;
}

/**
 * 
 * @param {*} data 
 * @param {*} socket 
 */
function update(data, socket) {

    if(players[hash[socket.id]] !== undefined) {
        //  use this to update movements
        players[hash[socket.id]].x = data.x;
        players[hash[socket.id]].y = data.y;
    }

    //  check if eaten a Spark
    ate(socket);

    //  check if within range of enemy

    //  check if within range of friend
    
}

function ate(socket) {

    let player = players[hash[socket.id]];

    if (range(this.sparks[s], _this.game.maxRadius)) {
        const sparkId = _this.sparks[s].id
        _this.sparks.splice(s, 1)
        this.$socket.emit('ate', sparkId)
    }
}

function range (other, maxRadius) {
    // console.log(other)
    //  if centers are closer than both radii added together then hit
    var x = Math.abs(this.pos.x - other.pos.x)
    var y = Math.abs(this.pos.y - other.pos.y)
    var hh = x * x + y * y
    var radii = (this.radius + other.radius) * (this.radius + other.radius)
    if (radii >= hh) {
      //  add a percentage of max range and this range
      var d = maxRadius - this.radius
      var p = d * 0.001
      this.radius += p
      this.score += other.radius
      this.power += other.radius
      // this.r += other.r * 0.2
      return true
    } else {
      return false
    }
  }

/**
 * 
 * @param {*} socket 
 */
function disconnect(socket) {
    console.log(socket.id + ' is disconnecting');

    //  get the index from the hash
    var r = hash[socket.id];

    //  remove from players
    if(players[r] !== undefined) {
        players.splice(r, 1);
    }

    //  remove from hash
    delete hash[socket.id];

    //  decrement all hash entries greater than r
    for (var i = 0; i < players.length; i++) {
        if (hash[i] > r) {
            hash[i]--;
        }
    }
}