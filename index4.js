var express = require('express');
var cors = require('cors');
var app = express();
app.use(cors());
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
uuidv4 = require('uuid/v4');

server.listen(3000, function() {
    console.log('Listening on :3000');
});


var players = [];
var hash  = {};

var sparks = [];

var game = {
    radius: 20,
    maxRadius: 100,
    arcRadius: 40,
    width: 3000,
    height: 3000,
    sparks: 3000
}

setInterval(heartBeat, 100);
function heartBeat() {
    // console.log(players);
    io.sockets.emit('heartbeat', { players, sparks });
}

function Player(x, y, r, a, id, n, w, h) {
    this.x  = x;
    this.y  = y;
    this.r  = r;
    this.a  = a;
    this.id = id;
    this.n = n
    this.w = w
    this.h = h
}

function Spark(x, y) {
    this.id = 
    this.x = x;
    this.y = y;
}

//  populate the sparks
for(var s = 0; s < game.sparks; s++) {
    sparks.push(new Spark(Math.floor(Math.random() * game.width), Math.floor(Math.random() * game.height), uuidv4()));
}

console.log(sparks);

io.sockets.on('connection', function(socket) {

    console.log('connected with socket id: ' + socket.id);

    socket.on('start', function (d) {
        //  get a random location
        var startX = getRandomInt(game.width);
        var startY = getRandomInt(game.height);
        var data = {
            player: {
                x: startX,
                y: startY,
                r: game.radius,
                a: game.arcRadius,
                id: socket.id,
                n: (d.name !== null) ? d.name : socket.id,
                w: d.game.width,
                h: d.game.height
            },
            game: {
                w: game.width,
                h: game.height,
                maxRadius: game.maxRadius
            }
        };

        //  send to client
        socket.emit('initial', data);
    
        players.push(new Player(data.player.x, data.player.y, data.player.r, data.player.a, socket.id, data.player.n));

        //  quick reference hash
        hash[socket.id] = players.length - 1;

        console.log('new', players);
    });

    socket.on('update', function(data) {
        for (var i = 0; i < players.length; i++) {
            if (socket.id == players[i].id) {
                players[i].x = data.x
                players[i].y = data.y
            }
        }

        //  check for in range

    });

    socket.on('ate', function(data) {
        for(var i = sparks.length - 1; i > 0; i--) {
            if(sparks[i].id == data) {
                sparks.splice(i, 1)
            }
        }
    })

    //  handle disconnect
    socket.on('disconnect', function() {
        //  get the index from the hash
        var r = hash[socket.id];

        //  remove from players
        if(players[r] !== undefined) {
            players.splice(r, 1);
        }

        //  remove from hash
        delete hash[r];

        //  decrement all hash entries greater than r
        for (var i = 0; i < players.length; i++) {
            if (hash[i] > r) {
                hash[r]--;
            }
        }
        // socket.broadcast.emit('players')

    });

});

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
