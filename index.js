require('dotenv').config();
var express = require('express');
var app = express();
var cors = require('cors');
app.use(cors());
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
uuidv4 = require('uuid/v4');

var port = process.env.PORT || 3000;

server.listen(port, function() {
    console.log('Listening on :' + port);
});

var players = [];
var playersHash = {};

//  global game variables
var game  = require("./objects/game");
var teams = require("./objects/teams");

//  Player class
var Player = require("./classes/Player.js");
//  Spark class
var Spark  = require("./classes/Spark.js");

//  the list of all active players sent to all players
setInterval(heartBeat, 1000);
function heartBeat() {
    //  update players with up-to-date list
    io.emit('heartbeat', { players: players.filter(function(v) {
        return v.playing;
    }) });
}

//  main socket event section
io.sockets.on('connection', function(socket)
{
    console.log(socket.id + ' is connecting ');

    //  send game data
    socket.emit('game', game);

    /**
     * PLAY
     */
    socket.on('play', (data) => socketPlay(data, socket));

    /**
     * DISCONNECT
     */
    socket.on('disconnect', (_) => socketDisconnect(socket));
});

/**
 * socketPlay
 * @param {*} socket 
 */
function socketPlay(data, socket)
{
    //  create server player
    var player = new Player(socket.id, game, data.canvas, getRandomTeam());
    players.push(player);
    
    //  add to hash
    playersHash[socket.id] = players.length - 1;
    
    console.log(players, playersHash);
}

/**
 * socketDisconnect
 */
function socketDisconnect(socket)
{
    console.log(socket.id + ' is disconnecting');

    //  get the index from the hash
    var r = playersHash[socket.id];

    //  remove from players
    if(players[r] !== undefined) {
        // players.splice(r, 1);
        players[r].changePlayState(false);
    }

    console.log(players, playersHash);
}

function getRandomTeam() {
    return teams[Math.floor(Math.random() * teams.length)];
}
