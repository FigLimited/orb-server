var teams = [
    { name: 'Red', hex: '#ff0000' },
    { name: 'Green', hex: '#00ff00' },
    { name: 'Blue', hex: '#0000ff' },
    { name: 'Pink', hex: '#FFB6C1' },
    { name: 'Orange', hex: '#ffa500' },
    { name: 'Black', hex: '#000000' },
];

module.exports = teams;
