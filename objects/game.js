var game = {
    radius: 20,
    maxRadius: 50,
    arcRadius: 200,
    width: 1000,
    height: 1000,
    sparks: 2,
    power: 100
};

module.exports = game;
