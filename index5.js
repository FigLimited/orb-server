var express = require('express');
var cors = require('cors');
var app = express();
app.use(cors());
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
uuidv4 = require('uuid/v4');

server.listen(3000, function() {
    console.log('Listening on :3000');
});





var players = [];
var hash = {};

var sparks = [];

var game = {
    radius: 10,
    maxRadius: 100,
    arcRadius: 40,
    width: 3000,
    height: 3000,
    sparks: 5000
}

setInterval(heartBeat, 25);
function heartBeat() {
    //  update players with up-to-date list
    io.emit('heartbeat', { players });

    //  check if new sparks aree needed
    if (sparks.length < game.sparks) {
        var spark = new Spark(Math.floor(Math.random() * game.width), Math.floor(Math.random() * game.height), uuidv4());
        sparks.push(spark);
        io.emit('spawned', spark);
    }
}

//  populate the sparks
for(var s = 0; s < game.sparks; s++) {
    sparks.push(new Spark(Math.floor(Math.random() * game.width), Math.floor(Math.random() * game.height), uuidv4()));
}

io.sockets.on('connection', function(socket) {

    console.log('connected with socket id: ' + socket.id);


    socket.on('start', function (d) {
        //  get a random location
        var startX = getRandomInt(game.width);
        var startY = getRandomInt(game.height);
        var data = {
            player: {
                x: startX,
                y: startY,
                r: game.radius,
                a: game.arcRadius,
                id: socket.id,
                n: (d !== undefined && d.name !== null) ? d.name : socket.id,
                w: d.game.width,
                h: d.game.height
            },
            game: {
                w: game.width,
                h: game.height,
                maxRadius: game.maxRadius
            },
            sparks
        };

        //  send to client
        socket.emit('initial', data);
    
        console.log('emmitted initial');

        players.push(new Player(data.player.x, data.player.y, data.player.r, data.player.a, socket.id, data.player.n, data.player.w, data.player.h));

        //  quick reference hash
        hash[socket.id] = players.length - 1;

    });

    /**
     * UPDATE
     */
    socket.on('update', function(data) {

        if(players[hash[socket.id]] !== undefined) {
            players[hash[socket.id]].x = data.x;
            players[hash[socket.id]].y = data.y;
        }

    });

    /**
     * ATE
     */
    socket.on('ate', function(sparkId) {

        for(var i = sparks.length - 1; i >= 0; i--) {
            if(sparks[i].id == sparkId) {
                sparks.splice(i, 1);
                break;
            }
        }

        // io.emit('eaten', sparkId);

    });

    /**
     * DISCONNECT
     */
    socket.on('disconnect', function() {

        //  get the index from the hash
        var r = hash[socket.id];

        //  remove from players
        if(players[r] !== undefined) {
            players.splice(r, 1);
        }

        //  remove from hash
        delete hash[r];

        //  decrement all hash entries greater than r
        for (var i = 0; i < players.length; i++) {
            if (hash[i] > r) {
                hash[r]--;
            }
        }

    });

});

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}