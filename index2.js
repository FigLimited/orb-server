var express = require('express');
var cors = require('cors');
var app = express();
app.use(cors());
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
uuidv4 = require('uuid/v4');
var port = 3000;

server.listen(process.env.PORT || port, function() {
    console.log('Listening on :' + process.env.PORT || port);
});

var Player = require("./classes/Player.js");
var Spark = require("./classes/Spark.js");

var players = [];
var hash = {};

var sparks = [];

var game = {
    radius: 20,
    maxRadius: 100,
    arcRadius: 200,
    width: 500,
    height: 500,
    sparks: 2
};

var teams = [
    { name: 'Red', hex: '#ff0000' },
    { name: 'Green', hex: '#00ff00' },
    { name: 'Blue', hex: '#0000ff' },
];

var weights = [1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 5];

setInterval(heartBeat, 25);
function heartBeat() {
    //  update players with up-to-date list
    io.emit('heartbeat', { players });

}

setInterval(sparkBeat, 1000);
function sparkBeat() {
    //  check if new sparks are needed
    if (sparks.length < game.sparks) {
        var spark = new Spark(
            Math.floor(Math.random() * game.width), 
            Math.floor(Math.random() * game.height),
            weights[Math.floor(Math.random() * weights.length)],
            uuidv4()
        );
        sparks.push(spark);
        io.emit('spawned', spark);
        console.log(sparks.length);
    }
}

io.sockets.on('connection', function(socket) {

    console.log('connected with socket id: ' + socket.id);



    /**
     * START
     */
    socket.on('start', function(data) {

        //  get a random location
        var startX = getRandomInt(game.width);
        var startY = getRandomInt(game.height);

        // get a random team
        var team = getRandomTeam();

        const returnData = {
            player: {
                x: startX,
                y: startY,
                radius: game.radius,
                arcRadius: game.arcRadius,
                team: team,
                id: socket.id,
                name: (data !== undefined && data.name !== null) ? data.name : makeId(8),
            },
            game: {
                width: game.width,
                height: game.height,
                radius: game.radius,
                maxRadius: game.maxRadius
            },
            sparks
        }

        //  send to client
        socket.emit('initial', returnData);

        //  create the player on the server
        players.push(new Player(returnData.player.x, returnData.player.y, returnData.player.radius, returnData.player.arcRadius, socket.id, returnData.player.name));

        //  quick reference hash
        hash[socket.id] = players.length - 1;

    });

    /**
     * UPDATE
     */
    socket.on('update', function(data) {

        if(players[hash[socket.id]] !== undefined) {
            players[hash[socket.id]].x = data.x;
            players[hash[socket.id]].y = data.y;
        }

    });

    /**
     * ATE
     */
    socket.on('ate', function(sparkId) {
        for(var i = sparks.length - 1; i >= 0; i--) {
            if(sparks[i].id.toString() == sparkId.toString()) {
                // console.log(sparks[i].id.toString(), sparkId.toString());
                sparks.splice(i, 1);
                // console.error(sparks.length);
                break;
            }
        }

        // io.emit('eaten', sparkId);

    });

    /**
     * DISCONNECT
     */
    socket.on('disconnect', function() {

        //  get the index from the hash
        var r = hash[socket.id];

        //  remove from players
        if(players[r] !== undefined) {
            players.splice(r, 1);
        }

        //  remove from hash
        delete hash[r];

        //  decrement all hash entries greater than r
        for (var i = 0; i < players.length; i++) {
            if (hash[i] > r) {
                hash[r]--;
            }
        }

    });

});


function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function getRandomTeam() {
    return teams[Math.random() * teams.length];
}

function makeId(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
